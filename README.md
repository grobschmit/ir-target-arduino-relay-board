# README #

This code is for creating a basic Arduino light that can be shot or distroyed. The Arduino needs needs to be connected to a relay 
on either pin 8 or 9. By using a relay you can control 12v, 240v or anything else supported by the relay. 

If the light is connected to the relay on pin 8 the relay/light will randomly flicker after being shot. 
If the light is connected to the relay on pin 9 the relay/light will toggle after being shot.

Note: To remain compatible with most laser tag systems this code does not full decode the IR signal, it just triggers after receiving 
a datapacket that is large enough to be a hit packet from most systems. This means that other signal may also trigger the unit (ie 
heal signals or TV remotes)

To reset, power cycle unit.

You can see the code here in action driving a 12v LED globe.
https://www.youtube.com/watch?v=yvWiMwTx5Ok