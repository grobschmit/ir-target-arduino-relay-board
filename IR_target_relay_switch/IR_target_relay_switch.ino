/*
Copyright (c) 2015 Bearocratic Designs (Robert Sturzbecher)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

This code is for creating a basic Arduino light that can be shot or distroyed. The Arduino needs needs to be connected to a relay 
on either pin 8 or 9. By using a relay you can control 12v, 240v or anything else supported by the relay. 

If the light is connected to the relay on pin 8 the relay/light will randomly flicker after being shot. 
If the light is connected to the relay on pin 9 the relay/light will toggle after being shot.

Note: To remain compatible with most laser tag systems this code does not full decode the IR signal, it just triggers after receiving 
a datapacket that is large enough to be a hit packet from most systems. This means that other signal may also trigger the unit (ie 
heal signals or TV remotes)

To reset, power cycle unit.

[Inputs]
-TSOP 3pin 56Mhz 

[Outputs]  
-LED  (optional)
-relay (optional)  flickering when shot
-relay (optional)  stable off when shot

*/


//includes
//#include <avr/power.h>                    //needed for disabling/enabling chip parts during sleep/wakeup 
//#include <avr/sleep.h>
//#include <avr/wdt.h>                      //watch dog timer, makes it easier to wake processor after time, not used currently
//#include <avr/interrupt.h>

//Pins
uint8_t IR_pin      = 2;                    //this is the data out pin of the TSOP   
uint8_t LED_pin     = 13;
uint8_t SPK_pin     = 2;
uint8_t relay_pin1  = 8;                    //this relay flickers after hit (suitible for lights)        
uint8_t relay_pin2  = 9;                    //this relay toggles when hit only          

uint8_t hit_state   = 0;                    //0=not hit, 1=hit


void setup() {
        Serial.begin(9600);                        //only works on Arduino boards
        pinMode(LED_pin, OUTPUT);
        pinMode(IR_pin, INPUT);
        pinMode(relay_pin1, OUTPUT);
        pinMode(relay_pin2, OUTPUT);
        digitalWrite(relay_pin1,LOW);              //on
        digitalWrite(relay_pin2,LOW);              //on
}


void loop() {
        IR_receive();                                   //check for IR signal
        if (hit_state ==1){
              uint8_t r = random(0,100);                //chance of blinking on, = 1 in 20 chance  
              if (r<5){  
                   digitalWrite(relay_pin1,LOW);        //will blink the light on
                   delay(r*20);                         //how long to blink on for  
              } else {
                   digitalWrite(relay_pin1,HIGH);       //will turn the light off
                   delay(30);
              }
        }
}

void Hit() {//This is what we do when we detect a hit
        digitalWrite(LED_pin,HIGH);
        hit_state =  1;
        digitalWrite(relay_pin1,HIGH);            //off
        digitalWrite(relay_pin2,HIGH);            //off
        Serial.println("Hit!"); 
        delay(50);
}



void IR_receive() {//we only need to check if we get more then 14 bits
        uint16_t IR_MAXPULSE                = 2600;                 // Sony (Miles) header is 2400us, 1= 1200us, 0= 600us  
        uint16_t IR_MAXPULSEGAP             = 750;                  // this is the timeout for the gaps between the bits and at the end, needs to be 600+tolerance  
        uint8_t  IR_RESOLUTION              = 32;                   // needs to be a multiple of 4 if 16mhz. 8 for 8Mhz, 32 for 1Mhz  
        uint8_t  IR_received_len            = 0;
        uint16_t highpulse, lowpulse;                               // temporary storage timing
        highpulse = lowpulse                = 0;                    // start out with no pulse length
        uint8_t currentpulse                = 0;                    // index for pulses we're storing   
        uint8_t timedout                    = 0; 
        uint16_t pulsetimeout               = IR_MAXPULSE / IR_RESOLUTION;
        uint16_t gaptimeout                 = IR_MAXPULSEGAP / IR_RESOLUTION;
        
        if(digitalRead(IR_pin) == LOW){                                          // If the receive pin is low a signal is being received.
            while (timedout == 0 ){
                 highpulse = lowpulse = 0;                                       // reset pulse timers 
                 //while (PIND & (1 << IRRX_pin)) {                              // pin is still HIGH, this is faster but less compatible
                 while (digitalRead(IR_pin) == HIGH) {                           // digitalRead is slower but more compatible if porting to attiny             
                       highpulse++;
                       delayMicroseconds(IR_RESOLUTION);
                       if (highpulse >= gaptimeout) {                            // If the pulse is too long, we 'timed out' - either nothing
                           timedout = 1;                                         // timedout so exit
                           //Serial.println(F(" Pulse timeout"));
                           //Serial.print(" Pulses: ");
                           //Serial.println(currentpulse);        
                           IR_received_len = currentpulse;  
                           if (currentpulse > 12){                               // Only process if enough data, should be header + 16 data bits for Milestag1 + parity, and about 14+ for MilesTag2 
                               Hit();                                            // call our hit function (Lights and buzzer)  
                           }
                           return;                                               // we've grabbed so far, and then reset
                       }
                }
                while (digitalRead(IR_pin) == LOW) {                             // pin is still LOW
                       lowpulse++;
                       delayMicroseconds(IR_RESOLUTION);
                       if (lowpulse >= pulsetimeout) {                           // If we get carrier that is too long to even be a header
                           //Serial.println(F("Pulse timeout. Signal too long, someone jamming/spamming us?"));
                           return;
                       }
                }
                currentpulse++;   
            }
        }
}

